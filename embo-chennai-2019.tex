\documentclass[10pt]{beamer}

% \usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{marvosym}
\usepackage{hyperref}
\usepackage{soul}
\usepackage{tikz}
\usepackage{wasysym}
\usepackage{booktabs}
\usepackage{colortbl}
%\usepackage{enumitem}

\usetikzlibrary{arrows,shapes.misc}

% setlist[itemize]{label={\footnotesize $\bullet$}, topsep=-0.2ex,
 %  partopsep=0pt, parsep=1pt, itemsep=0pt, leftmargin=3.5mm,
 %  labelsep=0.5ex} 

\newtheorem{thm}{Theorem}

\newcommand{\Rr}{\mathbb{R}}
\newcommand{\Nr}{\mathbb{N}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\R}{\mathcal{R}}
\newcommand{\Q}{\mathcal{Q}}
\renewcommand{\L}{\mathcal{L}}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\G}{\mathcal{G}}
\newcommand{\Hg}{\mathcal{H}}
\newcommand{\V}{\mathcal{V}}
\newcommand{\E}{\mathcal{E}}
\newcommand{\pa}{\text{Pa}}
\newcommand{\D}{\mathcal{D}}

\newcommand{\Hnull}{\mathcal{H}_{\text{null}}}
\newcommand{\Halt}{\mathcal{H}_{\text{alt}}}
\newcommand{\PT}[1]{$\text{P}_\text{#1}$}

%\newcommand{\strong}[1]{{\color{red}#1}}
\newcommand{\balph}{\boldsymbol{\alpha}}
\DeclareMathOperator*{\argmax}{argmax}



\definecolor{frias}{rgb}{0,0.616,0.580}%{0,0.627,0.627}%{0,0.729,0.71}

\definecolor{uibr}{rgb}{0.811,0.235,0.227}
\definecolor{uibg}{rgb}{0.541,0.671,0.404}

\newcommand{\strong}[1]{{\color{uibr}#1}}

\newcommand{\chm}{{\large \strong{\smiley}}}
\newcommand{\crs}{{\large\color{red}\frownie}}

\usetheme{Malmoe}
\usecolortheme[named=uibr]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{}
\setbeamertemplate{enumerate subitem}{{\normalsize(\alph{enumii})}}

\graphicspath{{./}{../../Logos/}{../../Figures/}{../../2012/CRI-2012/}}

\title{\textcolor{black}{Causal gene regulatory network inference}}

\subtitle{\textcolor{black}{\\{EMBO Symposium Regulatory Epigenomics}}}

\author{Tom Michoel}

\date{14 January 2019}

\begin{document}


\begin{frame}
  \thispagestyle{empty}

  \vspace*{-7mm}
  \hspace*{-10mm}\includegraphics[width=\paperwidth]{uib-header}
  

  \titlepage

  %\vspace*{-3mm}
  
\end{frame}


\begin{frame}
  \frametitle{Genetic variation influences gene regulation}

  \begin{center}
    \includegraphics[width=.8\linewidth]{pai_plosgenet2015_fig1_edit}

    {\tiny [Pai, Pritchard, Gilav. PLOS Genet (2015)]}
  \end{center}

  \medskip
  
  Genetic variation \emph{directly} affects DNA methylation, binding
  of epigenetic and transcription factors and 3D chromatin
  organization, leading to altered transcription.
\end{frame}

\begin{frame}
  \frametitle{Genetic variation informs on \emph{local} regulatory mechanisms}
   \begin{center}
    \includegraphics[width=\linewidth]{deplancke2016-fig2}

    {\tiny [Deplancke, Alpern, Gardeux. Cell (2016)]}
  \end{center}

  \medskip

  Coupling genetic variation data with information on TF binding
  events allows to predict functional effects of motif disruption and
  cooperative and collaborative TF binding mechanisms in a
  locus-specific manner.
\end{frame}

\begin{frame}
  \frametitle{Genetic variation informs on \emph{distal} regulatory mechanisms}

   \begin{center}
    \includegraphics[width=.67\linewidth]{nrg1964-f3-crop}\\
     {\tiny [Rockman and Kruglyak, Nat Rev Genet (2008)]}
   \end{center}

   \medskip

   Genotypes of regulatory variants are fixed from birth and act as
   \emph{experimental instruments} to inhibit or promote expression of
   regulatory factors \emph{independent} of environmental confounders.
 \end{frame}
 
\begin{frame}
  \frametitle{Systems genetics studies the local and distal effects of
    genetic variation on gene regulation and organismal phenotypes}

   \begin{center}
    \includegraphics[width=.8\linewidth]{albert2016-fig1B-2}\\
     {\tiny [Albert and Kruglyak, Nat Rev Genet (2016)]}
   \end{center}
\end{frame}

\begin{frame}
  \frametitle{Systems Genetics $=$ (Statistics)$^2$}

  \medskip
  
  \begin{columns}
    \begin{column}{.33\linewidth}
      \includegraphics[width=\linewidth]{GTEx_diagram}
      \begin{center}
        {\tiny [http://gtexportal.org]}        
      \end{center}
    \end{column}
    \begin{column}{.7\linewidth}
      Instead of direct experimental manipulation of DNA
      elements or  regulatory factors, study gene regulation by:\\[2mm]
      \begin{itemize}
      \item Statistical sampling of the \emph{causes} (genotypes) and
        \emph{consequences} (gene expression) of gene regulation
        using genome-wide measurements in unrelated individuals.\\[2mm]
      \item Mapping eQTLs $=$ expression QTLs $=$ genetic variants
        associated with variation in nearby ($\sim$1Mb) gene
        expression.\\[2mm]
      \item Using statistical and machine learning approaches to
        reconstruct local and distal regulatory mechanisms between
        eQTLs and their associated genes.
      \end{itemize}
    \end{column}
  \end{columns}
  \medskip
  \begin{center}
    \strong{\large  ``From large data to useful models.''}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Mendelian randomization enables causal inference}
  \fontsize{9}{11}\selectfont

  Randomized controlled trial $=$ randomly assign individuals to
  treatment groups, then measure difference in outcome.

 % Mendelian randomization:
  \begin{itemize}
  \item Random group assigment $=$ random segregation of eQTL alleles.
  \item Treatment $=$ high/low TX1 expression, depending on eQTL genotype.
  \item Outcome $=$ high/low TX2 expression.
  \end{itemize}
  \begin{center}
    \includegraphics[width=.7\linewidth]{nrg2612-f4}\\
    {\tiny [Mackay, Nat Rev Genet (2009)]}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Some examples of recent datasets in human}
  \fontsize{8}{10}\selectfont
  \begin{center}
    \textbf{Genotype $+$ Expression}\\[2mm]
    
    \hspace*{-10mm}
    \begin{tabular}{llcccl}
      \toprule
      \textbf{Name} & \textbf{Tissues} & $\mathbf{n_T}$ & $\mathbf{n_G}$ & $\mathbf{n_E}$ & \textbf{Reference}\\
      \midrule
      GEUVADIS & LCL & 1 & 465 & 465 & Lappalainen \emph{et al.} Nature (2013)\\
      STAGE & vasc, metab & 7 & 109 & 612 &  Foroughi Asl \emph{et al.} Circ Card Genet (2015)\\
      STARNET & vasc, metab & 7 & 566 & 3,786 &  Franz\'en \emph{et al.} Science (2016)\\
      BLUEPRINT & immune & 3 & 197 & 591 & Chen \emph{et al.} Cell (2016)\\
      HipSci & iPSC & 1 & 492 & 492 & Kilpinen \emph{et al.} Nature (2017) \\
      GTEx & whole body & 48 & 620 & 10,294 & GTEx Consortium. Nature (2017)\\
      \bottomrule
    \end{tabular}
  \end{center}

  \bigskip
  
  \begin{center}
    \textbf{Genotype $+$ Epigenome}\\[2mm]
    
    \hspace*{-8mm}
    \begin{tabular}{llcccl}
      \toprule
      \textbf{Study} & \textbf{Tissues} & $\mathbf{n_T}$ & $\mathbf{n_G}$ & $\mathbf{n_E}$ & \textbf{Reference}\\
      \midrule
      -- & LCL & 1 & 47 & 235 & Waszak \emph{et al.} Cell (2015) \\
      -- & LCL & 1 & 75 & 225 & Grubert \emph{et al.} Cell (2015) \\
      BLUEPRINT & immune & 3 & 197  & 1,043 & Chen \emph{et al.} Cell (2016) \\
      HipSci & iPSC & 1 & 441 & 441 & Kilpinen \emph{et al.} Nature (2017) \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{frame}
 \begin{frame}
  \frametitle{Some examples of recent datasets in human}
  \fontsize{8}{10}\selectfont
  \begin{center}
    \textbf{Genotype $+$ Expression}\\[2mm]
    
    \hspace*{-10mm}
    \begin{tabular}{llcccl}
      \toprule
      \textbf{Name} & \textbf{Tissues} & $\mathbf{n_T}$ & $\mathbf{n_G}$ & $\mathbf{n_E}$ & \textbf{Reference}\\
      \midrule
      GEUVADIS & LCL & 1 & 465 & 465 & Lappalainen \emph{et al.} Nature (2013)\\
      \rowcolor{yellow}
      STAGE & vasc, metab & 7 & 109 & 612 &  Foroughi Asl \emph{et al.} Circ Card Genet (2015)\\
      \rowcolor{yellow}
      STARNET & vasc, metab & 7 & 566 & 3,786 &  Franz\'en \emph{et al.} Science (2016)\\
      BLUEPRINT & immune & 3 & 197 & 591 & Chen \emph{et al.} Cell (2016)\\
      HipSci & iPSC & 1 & 492 & 492 & Kilpinen \emph{et al.} Nature (2017) \\
      GTEx & whole body & 48 & 620 & 10,294 & GTEx Consortium. Nature (2017)\\
      \bottomrule
    \end{tabular}
  \end{center}

  \bigskip
  
  \begin{center}
    \textbf{Genotype $+$ Epigenome}\\[2mm]
    
    \hspace*{-8mm}
    \begin{tabular}{llcccl}
      \toprule
      \textbf{Study} & \textbf{Tissues} & $\mathbf{n_T}$ & $\mathbf{n_G}$ & $\mathbf{n_E}$ & \textbf{Reference}\\
      \midrule
      -- & LCL & 1 & 47 & 235 & Waszak \emph{et al.} Cell (2015) \\
      -- & LCL & 1 & 75 & 225 & Grubert \emph{et al.} Cell (2015) \\
      BLUEPRINT & immune & 3 & 197  & 1,043 & Chen \emph{et al.} Cell (2016) \\
      HipSci & iPSC & 1 & 441 & 441 & Kilpinen \emph{et al.} Nature (2017) \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{The STAGE and STARNET studies}
  \begin{columns}
    \begin{column}{.5\linewidth}
      \fontsize{9}{11}\selectfont 

      Multi-organ expression profiling in 7 vascular and metabolic
      tissues of patients undergoing coronary artery bypass grafting
      surgery at Karolinska University Hospital/Tartu University
      Hospital\\[3mm]

      \textbf{STAGE}
      \begin{itemize}
      \item 109 individuals, SNP arrays
      \item 612 tissue expression arrays
      \end{itemize}
      {\fontsize{7}{9}\selectfont [Foroughi-Asl \textit{et al}, Circ Cardiovasc Genet 2015]}\\[3mm]
    
      \textbf{STARNET}
      \begin{itemize}
      \item 566 individuals, SNP arrays
      \item 3,786 tissue RNA-seq profiles
      \end{itemize}
      \qquad{\fontsize{7}{9}\selectfont [Franz\'en \textit{et al}, Science 2016]}
    \end{column}
    \begin{column}{.58\linewidth}
      \includegraphics[width=\linewidth]{aad6970-TableS1}
    \end{column}
  \end{columns}
\end{frame}



\begin{frame}
  \frametitle{Multi-tissue eQTLs are enriched for CAD risk variants}
  \begin{center}
    \begin{minipage}{.33\linewidth}
        \includegraphics[width=\linewidth]{hassan-fig1a}
      \end{minipage}~%
      \begin{minipage}{.33\linewidth}
        \includegraphics[width=\linewidth]{hassan-fig1d}
      \end{minipage}~%
      \begin{minipage}{.3\linewidth}
        \includegraphics[width=\linewidth]{hassan-fig2e1}
      \end{minipage}
      %\vspace*{-4mm}
      \begin{center}
        {\tiny [Foroughi Asl \textit{et al}, Circ Cardiovasc Genet (2015)]}
      \end{center}
    \end{center}
    \begin{itemize}
    \item Identified 29.530 \textit{cis}-eSNPs for 6,450 genes, 75\%
      are tissue-specific.
    \item Tissue-shared eSNPs are highly enriched for CAD risk
      variants and regulatory features.
    \end{itemize}
\end{frame}

\begin{frame}

  \vspace*{1mm}

  \hspace*{-4mm}\begin{minipage}{\linewidth}
    \includegraphics[width=1.1\linewidth]{cellsystems_title}
  \end{minipage}\\[2mm]


  \hspace*{-4mm}\begin{minipage}{.55\linewidth}
    \includegraphics[width=\linewidth]{cellsystems_graphabs}
  \end{minipage}~%
  \begin{minipage}{.45\linewidth}
    \includegraphics[width=.8\linewidth]{cellsystems_authors}\\[7mm]

    \includegraphics[width=1.2\linewidth]{cellsystems_highlights}
  \end{minipage}\\[1mm]

  \hspace*{-3mm}\includegraphics[width=.45\linewidth]{cellsystems_cite}

\end{frame}

\begin{frame}
  % \frametitle{Cross-tissue regulatory gene networks in coronary artery
  % disease}
\fontsize{8}{10}\selectfont 
  \begin{columns}
    \begin{column}{.5\linewidth}
      \vspace*{-4mm}
      \hspace*{-10mm}
      \begin{itemize}
      
      \item Multi-tissue clustering identified 171 co-expression
        clusters (94 tissue-specific/77 cross-tissue).
      \item 61 clusters associated to key CAD phenotypes (athero,
        cholesterol, glucose, CRP).
      \item 30 CAD-causal clusters (CAD risk enriched eSNPs/GWAS
        genes).
      \item 12 clusters conserved in mouse with same phenotype
        association in same tissue.
      \item Key drivers of athero-causal Bayesian gene networks
        validated by siRNA targeting in THP-1 foam cells.
      \end{itemize}
    \end{column}
    \begin{column}{.65\linewidth}
     

      \includegraphics[width=\linewidth]{Figure_1_Study_Overview}

     % \vspace*{-4mm}
      \begin{center}
        {\tiny [Talukdar \textit{et al}, Cell Systems (2016)]}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}


% \begin{frame}
%   \frametitle{STAGE eQTLs are enriched for regulatory variants}
% \end{frame}

\begin{frame}
  \frametitle{Network inference predicts CAD-causal networks}
    \fontsize{9}{11}\selectfont 
  \begin{columns}
    \begin{column}[t]{0.5\linewidth}
      \textbf{1.} AAW-specific cluster of RNA processing genes
      associates with stenosis score.
    \begin{center}
      \includegraphics[width=\linewidth]{BN42_human}
    \end{center}

    \medskip

    \textbf{2.} Coexpression is conserved in aorta of atherosclerotic mice and
    associates with aortic lesion size.
    \begin{center}
      \includegraphics[width=\linewidth]{BN42_mouse}  
%      {\tiny [Talukdar,\dots,\textbf{Michoel$^\ast$},Bj\"orkegren$^\ast$, 2015]}
    \end{center}
    \end{column}
    \begin{column}[t]{0.5\linewidth}
      \textbf{3.} siRNA KO of 4/7 key driver genes has significant
      effect in cell line model of atherosclerosis.
      \begin{center}
        \includegraphics[width=\linewidth]{Husain_fig3g.pdf}
      \end{center}

      \textbf{4.} Significant number of targets show differential
      expression upon siRNA KO of regulators.
    \end{column}
  \end{columns}

\end{frame}

% \begin{frame}
%   \frametitle{Network inference predicts TF targets}
% \end{frame}


\begin{frame}
  \frametitle{Causal inference using instrumental variables}
  \fontsize{9}{11}\selectfont 
  \begin{center}
    \includegraphics[width=.45\linewidth]{civelek_nrg_fig3a}\\[5mm]
    \fontsize{7}{11}\selectfont
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1,1) {$M_1$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (X) edge (Y);
  \end{tikzpicture} \quad\textit{vs.}\quad
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1,1) {$M_2$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (Y) edge (X);
  \end{tikzpicture}\; or\;
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (.5,1) {$M_3$};
    \node (X) at (1,0.5) {$A$};
    \node (Y) at (1,-0.5) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (E) edge (Y);
    %\path[<->,thick] (X) edge (Y);
  \end{tikzpicture}\; or\;
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1,1) {$M_4$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \path[->,thick] (E) edge[out=30,in=150] (Y);
    \path[->,thick] (Y) edge (X);
  \end{tikzpicture}
  \end{center}
  \begin{enumerate}
  \item \strong{\textit{Trans-association:}} Statistical dependence between $E_A$ and $B$ excludes model $M_2$.\\[2mm]
  \item \strong{\textit{Mediation:}} Statistical independence between $E_A$ and $B$ conditioned on $A$ excludes model $M_3$ and $M_4$.
  \end{enumerate}
  \begin{center}
    \strong{BUT}

    \bigskip

    Mediation fails in the presence of hidden confounders due to collider effect.
  \end{center}
  
  \begin{center}
    \fontsize{9}{11}\selectfont 
    \begin{tikzpicture}[baseline=2mm,scale=.8]
      \node[shape=circle,inner sep=1pt,draw] (X) at (1,0) {$A$};
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[->,thick] (E) edge (X);
      \path[->,thick] (X) edge (Y);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
      %\path[->,dashed,thick] (X) edge [out=60,in=120] (Y);
    \end{tikzpicture}\;$\Rightarrow$\;
    \begin{tikzpicture}[baseline=2mm,scale=.8]
%      \node[shape=circle,inner sep=1pt,draw] (X) at (1,0) {$A$};
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      %\path[->,thick] (E) edge (X);
      %\path[->,thick] (X) edge (Y);
      \path[-,thick,dashed] (H) edge (E);
      \path[->,thick,dashed] (H) edge (Y);
      %\path[->,dashed,thick] (X) edge [out=60,in=120] (Y);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Causal inference with hidden confounders}
  \fontsize{9}{11}\selectfont 
  \begin{center}
    \fontsize{9}{11}\selectfont 
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
      \node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_1$};
      \node (X) at (1,0) {$A$};
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[->,thick] (E) edge (X);
      \path[->,thick] (X) edge (Y);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
      %\path[->,dashed,thick] (X) edge [out=60,in=120] (Y);
    \end{tikzpicture}\quad\textit{vs.}\quad
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
      \node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_2$};
      \node (X) at (1,0) {$A$};
      \node (Y) at (2.,0) {$B$};
      \node (E) at (0.,0.) {$E_A$};
      \node (H) at (1.5,1) {$H$};
      \path[->,thick] (E) edge (X);
      %\path[->,thick] (X) edge (Y);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
      %\path[->,dashed,thick] (X) edge [out=60,in=120] (Y);
    \end{tikzpicture}\; or\;
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_3$};
    \node (X) at (1,0) {$A$};
    \node (Y) at (2,0) {$B$};
    \node (E) at (0,0) {$E_A$};
    \node (H) at (1.5,1) {$H$};
    \path[->,thick] (E) edge (X);
    \path[->,thick] (Y) edge (X);
    \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
  \end{tikzpicture}\; or\;
  \begin{tikzpicture}[baseline=-1mm,scale=.8]
    \node[shape=circle,inner sep=0pt,draw] (M) at (.5,2) {$M_4$};
    \node (X) at (1,0.5) {$A$};
    \node (Y) at (1,-0.5) {$B$};
    \node (E) at (0,0) {$E_A$};

    
    \path[->,thick] (E) edge (X);
    \path[->,thick] (E) edge (Y);
  \end{tikzpicture}
  \end{center}
  \begin{enumerate}
  \item \strong{\textit{Trans-association:}} Statistical dependence between $E_A$ and $B$ excludes model $M_2$ and $M_3$.\\[2mm]
  \item \strong{\textit{Non-independence:}} Statistical dependence between $A$ and $B$ conditioned on $E_A$ excludes model $M_4$.
  \end{enumerate}
  \vspace*{-5mm}
  \begin{center}
    \strong{BUT}
    
    \smallskip
    
    Non-independence fails if there is a hidden confounder in model $M_4$.\\[2mm]

    \fontsize{9}{11}\selectfont 
    \begin{tikzpicture}[baseline=-1mm,scale=.8]
      \node (X) at (1,0.5) {$A$};
      \node (Y) at (1,-0.5) {$B$};
      \node (E) at (0,0) {$E_A$};
      \node (H) at (2,0) {$H$};
      
      \path[->,thick] (E) edge (X);
      \path[->,thick] (E) edge (Y);
      \path[->,thick,dashed] (H) edge (X);
      \path[->,thick,dashed] (H) edge (Y);
    \end{tikzpicture}\\[2mm]

    \textit{Because enhancer-gene interactions are \textbf{local}, a
      direct effect of $E_A$ on $B$ can only occur if $A$ and $B$ are
      co-located on the genome.}

        
  \end{center}
\end{frame}

\begin{frame}
  \hspace*{-6mm}\includegraphics[width=1.1\linewidth]{findr_ploscb_header}
  
  \fontsize{8}{10}\selectfont 

  \begin{columns}
    \begin{column}{.58\linewidth}
     \includegraphics[width=1.2\linewidth]{findr_ploscb_title}\\[3mm]
      \begin{itemize}
      \item eQTLs act as control variables to infer causal
        direction between correlated expression traits.
       
      \item Findr software implements 6 likelihood-ratio tests and
        outputs probabilities (1-FDR) of hypotheses being true.\\
      \item Analytic results to avoid random permutations, resulting
        in massive speed-up.\\
      \item Combining LLR tests gives probabilities of causal effects.\\
      \end{itemize}
    \end{column}
    \begin{column}{.47\linewidth}
      % \includegraphics[width=\linewidth]{findr_ploscb_title}\\[3mm]
      \vspace*{-5mm}
       \begin{flushright}
          \includegraphics[width=.8\linewidth]{civelek_nrg_fig3a}\\%[2mm]
        \end{flushright}
      \begin{center}
        \includegraphics[width=\linewidth]{findr-tests} 
      \end{center}
    \end{column}
  \end{columns}

  \medskip

  \begin{minipage}{.5\linewidth}
    \bigskip
    \begin{center}
      \href{https://github.com/lingfeiwang/findr}{{\small
        \texttt{https://github.com/lingfeiwang/findr}}}
    \end{center}
  \end{minipage}\hfill% \hspace*{90mm}
  \begin{minipage}{.14\linewidth}
    \vspace*{-5mm}
    \begin{center}
      \includegraphics[width=\linewidth]{lingfei_wang}
    \end{center}
  \end{minipage}
  
  % \begin{center}
    

  %   
   
  % \end{center}
\end{frame}

\begin{frame}
  \frametitle{Findr accounts for weak secondary linkage, allows for
     hidden confounders, and outperforms existing methods} 
   \fontsize{9}{10.5}\selectfont 
   \begin{center}
     % DREAM5 Systems Genetics Challenge\\
     % {\footnotesize Synthetic networks (1000 genes), simulated data\\
     %    (RILs and ODE model with \textit{cis} and \textit{trans} effects)}

     \includegraphics[width=.55\linewidth]{fig1-DREAM}
   \end{center}
   \vspace*{-2mm}
   \begin{itemize}
   \item \textbf{Mediation:} high specificity, but low sensitivity due to hidden confounders, worse with increasing sample size.
   \item \textbf{Non-independent effects:} much higher sensitivity at potential cost of  
       increase in false positives.
   \end{itemize}

    %\smallskip

    \begin{center}
      \fbox{
        \begin{minipage}{.9\linewidth}
          \begin{center}
            In simulations and comparisons to known miRNA and
            TF-targets, ``non-independent effects'' always performed better 
            than ``mediation''.  
          \end{center}
        \end{minipage}
      }
    \end{center}

 \end{frame}

 \begin{frame}
  \frametitle{The STAGE and STARNET studies}
  \begin{columns}
    \begin{column}{.5\linewidth}
      \fontsize{9}{11}\selectfont 

      Multi-organ expression profiling in 7 vascular and metabolic
      tissues of patients undergoing coronary artery bypass grafting
      surgery at Karolinska University Hospital/Tartu University
      Hospital\\[3mm]

      \textbf{STAGE}
      \begin{itemize}
      \item 109 individuals, SNP arrays
      \item 612 tissue expression arrays
      \end{itemize}
      {\fontsize{7}{9}\selectfont [Foroughi-Asl \textit{et al}, Circ Cardiovasc Genet 2015]}\\[3mm]
    
      \textbf{STARNET}
      \begin{itemize}
      \item 566 individuals, SNP arrays
      \item 3,786 tissue RNA-seq profiles
      \end{itemize}
      \qquad{\fontsize{7}{9}\selectfont [Franz\'en \textit{et al}, Science 2016]}
    \end{column}
    \begin{column}{.58\linewidth}
      \includegraphics[width=\linewidth]{aad6970-TableS1}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Findr predicts causal pathways in CAD -- \textit{LDLR}}
  \fontsize{9}{11}\selectfont 
  Network predicted in \textbf{foam cells} -- type of macrophage, ingest LDL on blood vessel walls, involved in atherosclerotic plaque growth and inflammation.\\[4mm]
  \hspace*{-7mm} \begin{minipage}{.6\linewidth}
      \includegraphics[width=\linewidth]{LDLR_net_2}
    \end{minipage}~%
    \hspace*{-5mm} \begin{minipage}{.52\linewidth}
      \begin{itemize}
      \item \textit{LDLR} -- LDL receptor; critical role in regulating blood cholesterol level.\\[2mm]
      \item \textit{HMGCR} -- rate-limiting enzyme in cholesterol biosynthesis; inhibited by statins. \\[2mm]
      \item \textit{MMAB} -- enzyme involved in vitamin B12 metabolism; necessary for function of MCM (\textit{MUT}), which helps break down cholesterol. \\[2mm]
      \item 21/34 genes involved in cholesterol or lipid metabolism ($p=10^{-17}$).
      \end{itemize}
    \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Functional dissection of a GWAS locus for plasma cortisol}
  \begin{columns}
    \begin{column}{.7\linewidth}
      \begin{itemize}
      \item Elevated levels of plasma cortisol contribute to risk
        factors for CVD (hypertension, obesity, diabetes).
      \item GWAMA in 25,000 individuals has identified a single locus
        spanning \emph{SERPINA6/A1} at genome-wide significance.
      \item \emph{SERPINA6} codes for CBG, cortisol binding globulin,
        which is produced in the liver, binds to cortisol in blood for
        transport  to other tissues.
      \item Cortisol in tissues binds to GR, glucocorticoid receptor,
        a TF expressed in almost every cell, regulating developmental,
        metabolic and immune response genes.
      \end{itemize}
    \end{column}
    \begin{column}{.35\linewidth}
      \begin{center}
        \includegraphics[width=.8\linewidth]{CORNET-manhattan}\\
        {\tiny [Bolton \emph{et al}. PLOS Genet (2014)]}
        
        \medskip
        
        \includegraphics[width=\linewidth]{LZ-CORNET-GWAMA.png}
        {\tiny [Crawford, Walker. Unpublished data.]}
      \end{center}
      
    \end{column}
  \end{columns}
  \bigskip
  \begin{center}
    \fbox{
      \begin{minipage}{.9\linewidth}
        \begin{center}
          What are the tissue-specific consequences and effects on
          disease of genetic variation for plasma cortisol?
        \end{center}
      \end{minipage}
    }
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{\textit{Cis} and \textit{trans} effects of genetic
    variation for plasma cortisol}
   \fontsize{9}{11}\selectfont 
  \begin{columns}
    \begin{column}{.6\linewidth}
      \vspace*{2mm}
      \begin{itemize}
      \item GWAMA peak for cortisol overlaps with
        \textit{cis}-regulatory variants (eQTL) for \emph{SERPINA6} in
        liver.
      \item \emph{Trans} effects on gene expression observed in liver
        and adipose tissues.
      \item Filtered significant \emph{trans} genes for potential mediators
        of cortisol-responsive effects:
        \begin{itemize}
        \item GR ChIP-seq in adipocytes.
        \item Glucocorticoid response element in promoter.
        \item Differentially expressed upon dexamethasone treatment in
          mouse adipose.
        \end{itemize}
      \item Found 14 (liver), 13 (VAF), 11 (SF) candidates.
      \end{itemize}
      
      \end{column}
    \begin{column}{.4\linewidth}
%      \includegraphics[width=\linewidth]{LZ-CORNET-GWAMA.png}
      \vspace*{-10mm}
      \includegraphics[width=\linewidth]{LZ-CORNET-STARNET-Liver-SERPINA6.png}
      % \vspace*{-4mm}
      % \begin{center}
      %   \includegraphics[width=.75\linewidth]{CORNET_cis07_trans_075}
      % \end{center}
      

%       \vfill
      
%       \hfill\begin{minipage}{.3\linewidth}
% %    \vspace*{-2mm}
%     \begin{center}
%       \includegraphics[width=\linewidth]{sean_bankier}\\
%       {\tiny Sean Bankier}
%     \end{center}
%   \end{minipage}
    \end{column}
  \end{columns}


  \hspace*{57mm}
  \begin{minipage}{.5\linewidth}
    \vspace*{-10mm}
    \begin{flushright}
      \includegraphics[width=.65\linewidth]{CORNET_cis07_trans_075}\hfill
      \begin{minipage}{.25\linewidth}
        \vspace*{-20mm}
        \begin{center}
          \includegraphics[width=\linewidth]{sean_bankier}\\
          {\tiny Sean Bankier}    
        \end{center}
      \end{minipage}
      
    \end{flushright}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{A cortisol-responsive gene network in adipose tissue
    controlled by \emph{IRF2}}
  \begin{columns}
    \begin{column}{.6\linewidth}
      \begin{itemize}
      \item Subcutaneous adipose expression of Interferon Regulatory
        Factor 2, \emph{IRF2}, a TF, is associated with genetic
        variation for cortisol.
      \item \emph{IRF2} is bound by GR in adipose ChIP-seq data.
      \item Causal inference predicts that \emph{IRF2} regulates an adipose
        gene network.
      \item Predicted targets are enriched for GREs and IRF-family
        binding motifs.
      \end{itemize}
      \medskip
      \begin{minipage}{.25\linewidth}
        %\vspace*{-20mm}
        \begin{center}
          \includegraphics[width=\linewidth]{sean_bankier}\\
          {\tiny Sean Bankier}    
        \end{center}
      \end{minipage}
    \end{column}
    \begin{column}{.4\linewidth}
      \includegraphics[width=\linewidth]{STARNET-IRF2-boxplot}
      
      \includegraphics[width=\linewidth]{IRF2-net}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Outlook: Resolving pleiotropic effects due to shared
    regulatory sites}
  
  \fontsize{9}{11}\selectfont
  \begin{columns}
    \begin{column}{.62\linewidth}
      Existing causal inference methods do not cope well with
      \emph{shared regulatory sites}:
      \begin{itemize}
      \item About 10\% of \textit{cis}-eQTLs are linked to more than 1
        gene within the same locus
        % \begin{center}
        %   \includegraphics[width=.6\linewidth]{fig_causal_inf_shared_E}
        % \end{center}
      \item Shared genetic instruments are a given when modelling
        \emph{local} regulatory mechanisms from genetic, epigenetic
        and gene expression variation data.
      \end{itemize}
      Preliminary mathematical results using path analysis methods
      (Wright, 1921) resolve ambiguity if recombination rate between
      instruments is non-zero.
      \begin{center}
        \begin{tikzpicture}[baseline=-5mm]
          \node (X) at (1,0) {$X$};
          \node (Y) at (2.4,0) {$Y$};
          \node (Ex) at (0.,0.) {$E_X$};
          \node (Ez) at (0.,-1) {$E_Z$};
          \node (Z) at (1.,-1) {$Z$};
          \path[->,thick] (Ex) edge (X);
          \path[->,thick] (X) edge node[above] {$a$} (Y);
          \path[<->,dashed,thick] (X) edge [out=60,in=120]  (Y);
          \path[<->,dashed,thick] (Z) edge [out=-10,in=-100] (Y);
          \path[<->,thick,dashed] (Ex) edge [out=210,in=150] (Ez);
          \path[->,thick] (Ez) edge (Z);
          \path[->,thick] (Z) edge (Y);
          \path[->,thick] (Z) edge (X);
          \path[<->,thick,dashed] (X) edge [out=210,in=150] (Z);
        \end{tikzpicture}~%
        $a=\dfrac{\rho_{E_X,Y}-\rho_{E_X,E_Z}\rho_{E_Z,Y}}{\rho_{E_X,X}-\rho_{E_X,E_Z}\rho_{E_Z,X}}$
      \end{center}
    \end{column}
    \begin{column}{.38\linewidth}
      \vspace*{-7mm}
      \begin{center}
        \includegraphics[width=.8\linewidth]{tong-2017-fig2}

        {\tiny [Tong \emph{et al}.  PLOS Genet (2017)]}
      \end{center}
      \begin{center}
        \includegraphics[width=\linewidth]{hemani-2017-fig1}

        {\tiny [Hemani \emph{et al}.  PLOS Genet (2017)]}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

% \begin{frame}
%   \frametitle{Outlook: Causal network inference using summary
%     statistics}
%   \fontsize{9}{11}\selectfont
  
%   Existing methods require access to individual-level genotype data:
%   \begin{itemize}
%   \item 
%   \end{itemize}
% \end{frame}

\begin{frame}
  \frametitle{Conclusions}
  \begin{columns}
    \begin{column}{.63\linewidth}
      \begin{itemize}
      \item eQTL genotypes act as control variables to infer the
        direction of causality between correlated expression traits.\\[2mm]
      \item Graphical models $+$ biological priors can account for
        hidden confounders.\\[2mm]
      \item Causal gene networks predict functional targets of TFs and
        downstream effects of GWAS loci.\\[2mm]
      \item Resolving cause and effect of regulatory mechanisms within
        a single locus is a challenge.
      \end{itemize}

      \medskip
      
      \begin{minipage}{.3\linewidth}
         \begin{tikzpicture}[baseline=-1mm,scale=.8]
          %\node[shape=circle,inner sep=0pt,draw] (M) at (1.5,2) {$M_1$};
          \node (X) at (1,0) {$A$};
          \node (Y) at (2.,0) {$B$};
          \node (E) at (0.,0.) {$E_A$};
          \node (H) at (1.5,1) {$H$};
          \path[->,thick] (E) edge (X);
          \path[->,thick] (X) edge (Y);
          \path[->,thick,dashed] (H) edge (X);
          \path[->,thick,dashed] (H) edge (Y);
          % \path[->,dashed,thick] (X) edge [out=60,in=120] (Y);
        \end{tikzpicture}
      \end{minipage}\hfill
      \begin{minipage}{.5\linewidth}
        \begin{tikzpicture}[baseline=-5mm]
          \node (X) at (1,0) {$X$};
          \node (Y) at (2.4,0) {$Y$};
          \node (Ex) at (0.,0.) {$E_X$};
          \node (Ez) at (0.,-1) {$E_Z$};
          \node (Z) at (1.,-1) {$Z$};
          \path[->,thick] (Ex) edge (X);
          \path[->,thick] (X) edge node[above] {$a$} (Y);
          \path[<->,dashed,thick] (X) edge [out=60,in=120]  (Y);
          \path[<->,dashed,thick] (Z) edge [out=-10,in=-100] (Y);
          \path[<->,thick,dashed] (Ex) edge [out=210,in=150] (Ez);
          \path[->,thick] (Ez) edge (Z);
          \path[->,thick] (Z) edge (Y);
          \path[->,thick] (Z) edge (X);
          \path[<->,thick,dashed] (X) edge [out=210,in=150] (Z);
        \end{tikzpicture}
      \end{minipage}
    \end{column}
    \begin{column}{.4\linewidth}
      
      \includegraphics[width=\linewidth]{civelek_nrg_fig3a}

      \smallskip
      
      \includegraphics[width=\linewidth]{nrg1964-f3-crop}

      \smallskip
      
      \includegraphics[width=\linewidth]{IRF2-net}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Acknowledgements}
  \fontsize{9}{11}\selectfont
  \begin{columns}
    \begin{column}[t]{.33\linewidth}
      \begin{block}{Group members}
        \textbf{Sean Bankier}\\
        Pau Erola\\
        Tor Helleland\\
        Siddharth Jayaraman\\
        Ammar Malik\\
       \textbf{Lingfei Wang}\\     
      \end{block}
      \begin{block}{Edinburgh}
        Ruth Andrew\\
        Andy Crawford\\
        Filippo Menolascina\\
        Brian Walker\\
      \end{block}
    \end{column}
    \begin{column}[t]{.33\linewidth}
      \begin{block}{Ghent University}
        Pieter Audenaert
      \end{block}
      \begin{block}{ISMMS/Karolinska}
        Ariella Cohain\\        
        \textbf{Hassan Foroughi Asl}\\
        Oscar Franz\'en\\
        \textbf{Husain Talukdar}\\
        Eric Schadt\\
        Johan Bj\"orkegren\\
      \end{block}
    \end{column}
    \begin{column}[t]{.34\linewidth}
      \vspace*{-10mm}

      \hspace*{16mm}\begin{minipage}{.6\linewidth}
        \includegraphics[width=\linewidth]{UiB-emblem_gray}
      \end{minipage}

      \begin{flushright}
        
        \includegraphics[width=.35\linewidth]{British_Heart_Foundation}\\%[5mm]

        \includegraphics[width=.7\linewidth]{MRC}\\[2mm]
        
        \includegraphics[width=\linewidth]{bbsrc-colour}\\[2mm]

        \hspace*{-21mm}\includegraphics[height=.9cm]{NIH}\\[5mm]

        
      \end{flushright}
      \hspace*{-11mm}\textbf{{\large http://lab.michoel.info}}
    \end{column}
  \end{columns}
  \vspace*{-18mm}
  \hspace*{-4mm}\begin{minipage}{.27\linewidth}
    \includegraphics[width=\linewidth]{Cbu_logo_black_transparent}
  \end{minipage}
  
  %\vspace*{2mm}

  % \begin{center}
  %   \textbf{{\large http://lab.michoel.info}}
  % \end{center}
\end{frame}

\begin{frame}
  \frametitle{Computational Biology Unit at  UiB}
  \begin{columns}
    \begin{column}{.7\linewidth}
      %\vspace*{-5mm}
      \begin{itemize}
      \item 10 research groups, 5 joined in 2018.\\[2mm]
      \item Joint venture between departments of informatics, biology,
        chemistry, biomedicine and clinical medicine.\\[2mm]
      \item Head of Elixir Norway and part of international networks.\\[2mm]
      \item Underpinned by 225 MNOK investment by BFS, UiB,
        HelseBergen over next 10 years.
      \end{itemize}
       \begin{center}
         \strong{\large\url{www.cbu.uib.no}}
      \end{center}
    \end{column}
    \begin{column}{.4\linewidth}
      
      \includegraphics[width=\linewidth]{20190203_24.png}

      \smallskip
      
      \includegraphics[width=\linewidth]{CBU-staff-2019}
    \end{column}
  \end{columns}

  \medskip
  
  \begin{center}
    \fbox{
      \begin{minipage}{.45\linewidth}
        \begin{center}
          PhD/Postdoc positions available  
        \end{center}
      \end{minipage}
    }
  \end{center}
  
  \begin{center}
    \begin{minipage}{.18\linewidth}
      \includegraphics[width=\linewidth]{Cbu_logo_black_transparent}
    \end{minipage}\quad
    \begin{minipage}{.1\linewidth}
          \includegraphics[width=\linewidth]{BFS-logo_blue}
        \end{minipage}\;
      \begin{minipage}{.1\linewidth} \includegraphics[width=\linewidth]{ELIXIR_logo_white_background-e1486455223471}
      \end{minipage}~%
      \begin{minipage}{.55\linewidth}
        \includegraphics[width=\linewidth]{UiBlogo_Eng_gray_h_V8}
      \end{minipage}
  \end{center}
\end{frame}

% \begin{frame}
%    \begin{quote}
%      All the impressive achievements of deep learning amount to just
%      curve fitting. To build truly intelligent machines, teach them
%      cause and effect.
%   \end{quote}
%   \begin{flushright}
%     Judea Pearl (2018)
%   \end{flushright}
% \end{frame}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
